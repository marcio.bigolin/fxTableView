/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.marciobigolin.livros.model;

public abstract class AbstractDAO<T> {
    public abstract T getOne();
    
    public abstract int delete(T obj);
}

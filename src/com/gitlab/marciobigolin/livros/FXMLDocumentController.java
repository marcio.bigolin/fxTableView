/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.marciobigolin.livros;

import com.gitlab.marciobigolin.livros.model.Livro;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author docente
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private TableView<Livro> tabela;
    @FXML
    private TableColumn<Livro, Integer> isbnLivroCol;
    @FXML
    private TableColumn<Livro, String> tituloLivroCol;
   
   private ObservableList<Livro> livros;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("Implemente o Cadastrar");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        livros = tabela.getItems();
        
        adicionaLivros();
          
        isbnLivroCol.setCellValueFactory(new PropertyValueFactory<>("isbn"));      
        tituloLivroCol.setCellValueFactory(new PropertyValueFactory<>("titulo"));  
                
        this.tabela.setItems(livros);
    }    
    
    
   
    private void adicionaLivros(){
        Livro l = new Livro();
        l.setIsbn(2121);
        l.setTitulo("A ponte do rio que cai");
        livros.add(l);
        
    }
}
